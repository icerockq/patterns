<?php
namespace collections\iMenu;

use Iterator;

interface Menu{
    public function createIterator() : Iterator;
}