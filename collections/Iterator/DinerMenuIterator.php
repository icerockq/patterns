<?php
namespace collections\iterator;

use InvalidArgumentException;
use Iterator;

class DinerMenuIterator implements Iterator{

    private $list;
    private $position = 0;

    public function __construct($menuItem){
        $this->list = $menuItem;
    }

    public function next(){
        $this->position++;
    }

    public function current(){
        return $this->list[$this->position];
    }

    public function valid(){
        if($this->position >= count($this->list) || $this->list[$this->position] == null){
            return false;
        }else{
            return true;
        }
    }

    public function remove(){
        if($this->position <= 0){
            throw new InvalidArgumentException ('Такого элемента нету, мы его не можем удалить');
        }
        if($this->list[$this->position - 1] != null){
            for ($i = $this->position - 1; $i < count($this->list) - 1; $i++){
                $this->list[$i] = $this->list[$i + 1];
            }
            $this->list[count($this->list) - 1] = null;
        }
    }

    public function key(){
        // TODO: Implement key() method.
    }
    public function rewind(){
        // TODO: Implement rewind() method.
    }
}