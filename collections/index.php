<?php
namespace collections;

require_once dirname(__DIR__, 1).'/autoload.php';

use collections\menu\DinerMenu;
use collections\menu\PancakeHouseMenu;

$pancakeHouseMenu = new PancakeHouseMenu();
$dinerMenu = new DinerMenu();

$waitress = new Waitress($pancakeHouseMenu, $dinerMenu);
$waitress->printMenu();
