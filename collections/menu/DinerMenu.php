<?php
namespace collections\menu;

use collections\iMenu\Menu;
use collections\iterator\DinerMenuIterator;
use collections\MenuItem;
use Iterator;

class DinerMenu implements Menu {
    const MAX_ITEMS = 6;
    private $numberOfItems = 0;
    private $menuItems;

    public function __construct(){
        $this->addItem('Vegetarian BLT', '(Fakin) Bacon with lettuce & tomato on whole wheat', true, 2.99);
        $this->addItem('BLT', 'Bacon with lettuce & tomato on whole wheat', false, 2.99);
        $this->addItem('Soup of the day', 'Soup of the day, with a side of potato salad', false, 3.29);
        $this->addItem('Hotdog', 'A hot dog, with saurkraunt, relish, onions, topped, with cheese', false, 3.05);
    }

    public function addItem(String $name, String $description, bool $vegetarian, float $price){
        $menuItem = new MenuItem($name, $description, $vegetarian, $price);
        if($this->numberOfItems >= self::MAX_ITEMS){
            echo 'Массив полон';
        }else{
            $this->menuItems[$this->numberOfItems] = $menuItem;
            $this->numberOfItems++;
        }
    }

    public function createIterator() : Iterator{
        return new DinerMenuIterator($this->menuItems);
    }
}