<?php
namespace collections\menu;

use ArrayObject;
use collections\iMenu\Menu;
use collections\MenuItem;
use Iterator;

class PancakeHouseMenu implements Menu {
    private $menuItems;

    public function __construct(){
        $this->menuItems = new ArrayObject();

        $this->addItem('K&B Pancake Breakfast', 'Pancake with scrambled eggs, and toast', true, 2.99);
        $this->addItem('Regular Pancake Breakfast', 'Pancake with fried eggs, sausage', false, 2.99);
        $this->addItem('Bluebarry Pancakes', 'Pancakes made with fresh blueberries', true, 3.49);
        $this->addItem('Waffles', 'Waffles, with your choice of blueberries or strawberries', true, 3.59);
    }

    public function addItem(String $name, String $description, bool $vegetarian, float $price){
        $menuItem = new MenuItem($name, $description, $vegetarian, $price);
        $this->menuItems->append($menuItem);
    }

    public function createIterator(): Iterator
    {
        return $this->menuItems->getIterator();
    }
}