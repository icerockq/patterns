<?php
namespace collections;

class MenuItem{
    private $name;
    private $description;
    private $vegetarian;
    private $price;

    public function __construct(String $name, String $description, bool $vegetarian, float $price){
        $this->name = $name;
        $this->description = $description;
        $this->vegetarian = $vegetarian;
        $this->price = $price;
    }

    public function getName(): String{
        return $this->name;
    }

    public function getDescription(): String{
        return $this->description;
    }

    public function isVegetarian(): bool{
        return $this->vegetarian;
    }

    public function getPrice(): float{
        return $this->price;
    }

}