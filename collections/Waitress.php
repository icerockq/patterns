<?php
namespace collections;

use collections\iMenu\Menu;
use Iterator;

class Waitress{
    private $pancakeHouseMenu;
    private $dinerMenu;

    public function __construct(Menu $pancakeHouseMenu, Menu $dinerMenu){
        $this->pancakeHouseMenu = $pancakeHouseMenu;
        $this->dinerMenu = $dinerMenu;
    }

    public function printMenu(){
        $pancakeIterator = $this->pancakeHouseMenu->createIterator();
        $dinerIterator = $this->dinerMenu->createIterator();

        echo '------- MENU BREAKFAST</br>';
        $this->_printMenu($pancakeIterator);
        echo '------- LUNCH</br>';
        $this->_printMenu($dinerIterator);
    }

    private function _printMenu(Iterator $iterator){
        while ($iterator->valid()){
            $menuItem = $iterator->current();
            echo $menuItem->getName().' + ';
            echo $menuItem->getPrice().' + --';
            echo $menuItem->getDescription().' + </br>';
            $iterator->next();
        }
    }
}