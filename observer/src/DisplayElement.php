<?php

namespace observer\src;


interface DisplayElement{
    public function display();
}