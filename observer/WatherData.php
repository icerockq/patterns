<?php
namespace observer;


use src\observer\Observer;
use src\observer\Subject;



class WatherData implements Subject {

    private $observes;
    private $temperature;
    private $humadity;
    private $pressure;

    public function __construct(){
        $this->observes = new ArrayList(Observer::class);
    }

    public function registerObserver(Observer $o){
        $this->observes->add($o);
    }

    public function removeObserver(Observer $o){
        $i = $this->observes->indexOf($o);
        if($i > 0){
            $this->observes->remove($i);
        }
    }

    public function notifyObservers(){
        for ($i = 0; $i < $this->observes->size(); $i ++){
            $observer = $this->observes->get($i);
            $observer->update();
        }
    }
}