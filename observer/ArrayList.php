<?php
namespace observer;



class Item{

}

class ArrayList{

    private $className;
    private $classes = [];

    public function __construct(string $className){
        $this->className = $className;
    }

    public function add(object $objectClass){
        try {
            if ($this->validationObject($objectClass)) {
                $this->classes[] = $objectClass;
            }
        } catch (\Exception $e) {
            echo $e;
        }
    }

    /**
     * @param object $object
     * @return bool
     * @throws \Exception
     */
    private function validationObject(object $object){
        if($object instanceof $this->className){
            return true;
        }else{
            throw new \Exception('This class is not a class : '.get_class($object));
        }
    }

    public function indexOf(object $object) : int {
        try {
            if ($this->validationObject($object)) {
                foreach ($this->classes as $key => $class) {
                    if ($class === $object) {
                        return $key;
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e;
        }
        return null;
    }

    public function remove(int $i){
        if($this->classes[$i]){
            unset($this->classes[$i]);
            $this->classes = array_values($this->classes);
        }else{
            new \Exception('No object in list with index '.$i.' found');
        }

    }

    public function size() : int {
        return intval(count($this->classes));
    }


    public function get(int $i) : object {
        if($this->classes[$i]){
            return $this->classes[$i];
        }
    }

}