<?php

namespace duck_strategy\quack;


class MuteQuack implements QuackBehavior
{

    public function quack()
    {
        echo('<<Silence>></br>');
    }
}