<?php

namespace duck_strategy\quack;


class Quack implements QuackBehavior
{

    public function quack()
    {
        echo('Quack</br>');
    }
}