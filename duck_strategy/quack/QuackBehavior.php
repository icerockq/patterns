<?php
namespace duck_strategy\quack;

interface QuackBehavior{
    public function quack();
}