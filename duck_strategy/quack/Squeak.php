<?php

namespace duck_strategy\quack;


class Squeak implements QuackBehavior
{

    public function quack()
    {
        echo('Squeak</br>');
    }
}