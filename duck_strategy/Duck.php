<?php
namespace duck_strategy;


use duck_strategy\fly\FlyBehavior;
use duck_strategy\quack\QuackBehavior;

abstract class Duck{

    protected $flyBehavior;
    protected $quackBehavior;

    public function __construct(){

    }


    public function setFlyBehavior(FlyBehavior $flyBehavior)
    {
        $this->flyBehavior = $flyBehavior;
    }


    public function setQuackBehavior(QuackBehavior $quackBehavior)
    {
        $this->quackBehavior = $quackBehavior;
    }


    public function getFlyBehavior(): FlyBehavior{
        return $this->flyBehavior;
    }

    public function getQuackBehavior() : QuackBehavior{
        return $this->quackBehavior;
    }


    public function performFly(){
        $this->getFlyBehavior()->fly();
    }

    public function performQuack(){
        $this->getQuackBehavior()->quack();
    }

    public abstract function display();

    public function swim(){
        echo 'All ducks float, even decoys</br>';
    }

}