<?php
namespace duck_strategy\fly;


interface FlyBehavior{
    public function fly();
}