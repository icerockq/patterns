<?php
namespace duck_strategy\fly;


class FlyWithWings implements FlyBehavior {

    public function fly(){
        echo('I`m flaying!!</br>');
    }
}