<?php

namespace duck_strategy\fly;


class FlyRocketPowered implements FlyBehavior {

    public function fly()
    {
        echo 'I`m flying with a rocket!</br>';
    }
}