<?php
namespace duck_strategy;


use duck_strategy\fly\FlyNoWay;
use duck_strategy\quack\Quack;

class ModelDuck extends Duck {

    public function __construct(){
        $this->flyBehavior = new FlyNoWay();
        $this->quackBehavior = new Quack();
    }

    public function display(){
       echo 'I`m a model duck</br>';
    }
}