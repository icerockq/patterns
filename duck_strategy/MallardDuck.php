<?php
namespace duck_strategy;


use duck_strategy\fly\FlyWithWings;
use duck_strategy\quack\Quack;

class MallardDuck extends Duck {

    public function __construct(){
        $this->quackBehavior = new Quack();
        $this->flyBehavior = new FlyWithWings();
    }

    public function display(){
        echo 'I`m a real Mallard duck</br>';
    }
}