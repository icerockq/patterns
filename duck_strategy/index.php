<?php
namespace duck_strategy;

use duck_strategy\fly\FlyRocketPowered;

require_once dirname(__DIR__, 1).'/autoload.php';

$mallard = new MallardDuck();
$mallard->performQuack();
$mallard->performFly();


$model = new ModelDuck();
$model->performFly();
$model->setFlyBehavior(new FlyRocketPowered());
$model->performFly();
